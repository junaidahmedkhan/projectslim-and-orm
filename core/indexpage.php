<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
	<base href="<?php echo 'http://localhost/junaid/projectslimwithparis/'; ?>" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="core/style.css">
</head>
<body>
<div class='main-container col-md-10 col-md-offset-1'>
	<div class='panel panel-default'>
		<div class="panel-heading">PROJECT OF SLIM AND PARIS</div>
		<table class="table table-striped"> 
			<thead> 
				<tr>  
					<th>ID</th> 
					<th>VALUE</th> 
					<th>ACTION</th> 
					<th>ACTION</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach ($get_data as $value) { ?>
				<tr>  
					<td><?php echo $value->id; ?></td> 
					<td id='val'><?php echo $value->value; ?></td> 
					<td><span class='update btn btn-success btn-xs'>Edit<i><?php echo $value->id; ?></i></span></td> 
					<td><a class='btn btn-danger btn-xs' href='del/<?php echo $value->id; ?>'> DELETE</a></td> 
				</tr> 
				<?php } ?>
			</tbody> 
		</table>
	</div>
</div>
<form method='post' action="save">
	<div class='col-md-4 col-md-offset-4 form'>
		<div class='form-group'>
			<input id='inputvalue' class='form-control' type='text' name='value'>
		</div >
		<input type='submit' class='btn btn-primary' name='submit'>
	</div>
</form>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="core/jquery.js"></script>
</body>
</html>

