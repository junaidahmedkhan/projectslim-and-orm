<?php
require 'Slim/slim.php';
require_once 'idiorm.php';
require_once 'paris.php';
ORM::configure('mysql:host=localhost;dbname=first_todo');
ORM::configure('username', 'root');
ORM::configure('password', 'root');

class todo extends Model {
    public static $_table = 'todo';
}	

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$view = $app->view();
$view->setTemplatesDirectory('core/');

// read data

$app->get('/', function () use ($app,$view) {
	$get_data = Model::factory('todo')->find_many();
    $app->render('indexpage.php', array('get_data' => $get_data));
})->name('home');

// delete data

$app->get('/del/:id', function ($id) use ($app) {
	try{	
		$delete = Model::factory('todo')->find_one($id);
		if(!$delete){
			throw new Exception("Not working");	
		}else{
			$delete->delete();
		}
	}catch(Exception $e){
		echo 'Message ' . $e->getMessage();
	}
	$app->redirect($app->urlFor('home'));
});

// insert data

$app->post('/save', function () use ($app){
	$value = $_POST['value'];
	if (isset($_POST['id'])) {
		$id = $_POST['id'] . $value;
		$create = Model::factory('todo')->find_one($id);
		$create->value = $value;
		$create->save();
	}else{
		$create = Model::factory('todo')->create();
		$create->value = $value;
		$create->save();
	}
	$app->redirect($app->urlFor('home'));
});

$app->run();
?>